<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="//message/customer/id = '' or not(//message/customer/id)">
                <xsl:apply-templates select="message"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="newstateId2"/>
                <!--   para lo de las fechas   -->
                <xsl:variable name="fecha" select="current-dateTime()"/>
                <xsl:variable name="fecha2"
                    select="format-dateTime($fecha, '[Y0001]-[M01]-[D01]:T[H01]:[m01]:[s001]Z')"/>
                <xsl:variable name="newFecha" select="$fecha + xs:dayTimeDuration('PT6H')"/>
                <xsl:variable name="newFecha2" select="$newFecha + xs:dayTimeDuration('PT7H')"/>
                <xsl:variable name="fecha2"
                    select="format-dateTime($newFecha, '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01].[f001]Z')"/>
                <xsl:variable name="fecha3"
                    select="format-dateTime($newFecha2, '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01].[f001]Z')"/>
                <!--   End   -->
                <soapenv:Envelope
                    xmlns:con="http://xmlns.oracle.com/apps/cdm/foundation/parties/contactPointService/"
                    xmlns:con1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/contactPoint/"
                    xmlns:org="http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/"
                    xmlns:org1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/organization/"
                    xmlns:org2="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/orgContact/"
                    xmlns:par="http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/"
                    xmlns:par1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/partySite/"
                    xmlns:per="http://xmlns.oracle.com/apps/cdm/foundation/parties/personService/"
                    xmlns:per1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/person/"
                    xmlns:rel="http://xmlns.oracle.com/apps/cdm/foundation/parties/relationshipService/"
                    xmlns:rel1="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/relationship/"
                    xmlns:sal="http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService/"
                    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                    xmlns:sour="http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/sourceSystemRef/"
                    xmlns:typ="http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService/types/">
                    <soapenv:Header>
                        <wsse:Security
                            xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                            xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
                            soapenv:mustUnderstand="1">
                            <wsu:Timestamp wsu:Id="TS-52B582EE998DDD491A15724674391604">
                                <wsu:Created>
                                    <xsl:value-of select="$fecha2"/>
                                </wsu:Created>
                                <wsu:Expires>
                                    <xsl:value-of select="$fecha3"/>
                                </wsu:Expires>
                            </wsu:Timestamp>
                            <wsse:UsernameToken
                                wsu:Id="UsernameToken-B71EDCAA808C3D785C157238295902614">
                                <wsse:Username>sergio.obregon@avansaas.com</wsse:Username>
                                <wsse:Password
                                    Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"
                                    >Oracle1234</wsse:Password>
                                <wsse:Nonce
                                    EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"
                                    >AJaqZNvSzc/z7yC6fvB67w==</wsse:Nonce>
                                <wsu:Created>
                                    <xsl:value-of select="$fecha2"/>
                                </wsu:Created>
                            </wsse:UsernameToken>
                        </wsse:Security>
                    </soapenv:Header>
                    <soapenv:Body>
                        <typ:updateSalesParty>
                            <typ:salesParty>

                                <sal:PartyId>
                                    <xsl:value-of select="//customer/id"/>
                                </sal:PartyId>
                                <sal:OrganizationParty>

                                    <org:PartyId>
                                        <xsl:value-of select="//customer/id"/>
                                    </org:PartyId>
                                    <org:OrganizationProfile>
                                        <xsl:if test="//customer/masterID">
                                            <org:IDMaster_c>
                                                <xsl:value-of select="//customer/masterID"/>
                                            </org:IDMaster_c>
                                            <org:IDClienteMaster_c>
                                                <xsl:value-of select="//customer/masterID"/>
                                            </org:IDClienteMaster_c>
                                        </xsl:if>

                                        <xsl:if
                                            test="//customer/recordControl/masterCreationDate != ''">
                                            <org:FechaDeAltaMASTER_c>
                                                <xsl:choose>
                                                  <xsl:when
                                                  test="//customer/recordControl/masterCreationDate != ''">
                                                  <xsl:variable name="fech"
                                                  select="//customer/recordControl/masterCreationDate"/>
                                                  <xsl:variable name="date"
                                                  select="replace($fech, '\s', '#')"/>
                                                  <xsl:variable name="dat"
                                                  select="substring-before($date, '#')"/>
                                                  <xsl:variable name="tmp"
                                                  select="substring-after($date, '#')"/>
                                                  <xsl:variable name="year"
                                                  select="substring($dat, 7)"/>
                                                  <xsl:variable name="mes"
                                                  select="substring($dat, 4, 2)"/>
                                                  <xsl:variable name="dia"
                                                  select="substring($dat, 0, 3)"/>
                                                  <xsl:variable name="newDate"
                                                  select="concat($year, '-', $mes, '-', $dia)"/>
                                                  <xsl:variable name="timeStamp"
                                                  select="dateTime(xs:date($newDate), xs:time($tmp))"/>
                                                  <xsl:variable name="newFecha"
                                                  select="$timeStamp + xs:dayTimeDuration('PT6H')"/>
                                                  <xsl:variable name="fecha2" as="xs:string"
                                                  select="format-dateTime($newFecha, '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01].[f001]Z')"/>
                                                  <xsl:value-of select="$fecha2"/>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </org:FechaDeAltaMASTER_c>
                                        </xsl:if>


                                        <xsl:if test="//customer/thirdCustomerType != ''">
                                            <org:TipoDeCliente_c>
                                                <xsl:value-of select="//customer/thirdCustomerType"
                                                />
                                            </org:TipoDeCliente_c>
                                        </xsl:if>

                                        <xsl:if
                                            test="//customer/locations/location/address/country/name != ''">
                                            <org:Pais_c>
                                                <xsl:value-of
                                                  select="//customer/locations/location/address/country/name"
                                                />
                                            </org:Pais_c>
                                        </xsl:if>


                                        <xsl:if test="//customer/businessName != ''">
                                            <org:NombreComercial_c>
                                                <xsl:value-of select="//customer/businessName"/>
                                            </org:NombreComercial_c>
                                        </xsl:if>
                                        <xsl:if test="//customer/fiscalName != ''">
                                            <org:OrganizationName>
                                                <xsl:value-of select="//customer/fiscalName"/>
                                            </org:OrganizationName>
                                        </xsl:if>
                                        <xsl:if test="//customer/status/id">
                                            <org:Status>
                                                <xsl:value-of select="//customer/status/id"/>
                                            </org:Status>
                                        </xsl:if>

                                        <xsl:if test="//customer/thirdCustomerType != ''">
                                            <org:TipoDeCliente_c>
                                                <xsl:value-of select="//customer/thirdCustomerType"
                                                />
                                            </org:TipoDeCliente_c>
                                        </xsl:if>
                                        <xsl:if test="//customer/rfc != ''">
                                            <org:RFC_c>
                                                <xsl:value-of select="//customer/rfc"/>
                                            </org:RFC_c>
                                        </xsl:if>

                                        <xsl:if
                                            test="//customer/locations/location/address/exteriorNumber != ''">
                                            <org:NumeroExterior_c>
                                                <xsl:value-of
                                                  select="//customer/locations/location/address/exteriorNumber"
                                                />
                                            </org:NumeroExterior_c>
                                        </xsl:if>
                                        <xsl:if
                                            test="//customer/locations/location/address/interiorNumber != ''">
                                            <org:NumeroInterior_c>
                                                <xsl:value-of
                                                  select="//customer/locations/location/address/interiorNumber"
                                                />
                                            </org:NumeroInterior_c>
                                        </xsl:if>
                                      
                                        <xsl:if
                                            test="//customer/locations/location/address/state != ''">
                                            <xsl:variable name="estadoId"
                                                select="//customer/locations/location/address/state/id"/>
                                            <xsl:variable name="cantDi"
                                                select="string-length($estadoId)"/>
                                            <org:Estado_c>
                                                <xsl:value-of select="//customer/locations/location/address/state/id"/>
                                            </org:Estado_c>
                                        </xsl:if>
                                       
                                     
                                        <xsl:if
                                            test="//customer/locations/location/address/street != ''">
                                            <org:Direccion_c>
                                                <xsl:value-of
                                                  select="//customer/locations/location/address/street"
                                                />
                                            </org:Direccion_c>
                                        </xsl:if>
                                        <xsl:if
                                            test="//customer/locations/location/address/postalCode != ''">
                                            <org:CodigoPostal_c>
                                                <xsl:value-of
                                                  select="//customer/locations/location/address/postalCode"
                                                />
                                            </org:CodigoPostal_c>
                                        </xsl:if>
                                        <xsl:if
                                            test="//customer/locations/location/address/neighborhood != ''">
                                            <org:Colonia_c>
                                                <xsl:value-of
                                                  select="//customer/locations/location/address/neighborhood"
                                                />
                                            </org:Colonia_c>
                                        </xsl:if>
                                        <xsl:if
                                            test="//customer/locations/location/address/country/name != ''">
                                            <org:Pais_c>
                                                <xsl:value-of
                                                  select="//customer/locations/location/address/country/name"
                                                />
                                            </org:Pais_c>
                                        </xsl:if>
                                        <xsl:if
                                            test="//customer/paymentConfig/payment/paymentMethod != ''">
                                            <org:MetodoDePagoSAT_c>
                                                <xsl:value-of
                                                  select="//customer/paymentConfig/payment/paymentMethod/id"
                                                />
                                            </org:MetodoDePagoSAT_c>
                                        </xsl:if>
                                        <xsl:if
                                            test="//customer/paymentConfig/payment/cfdiUse != ''">
                                            <org:UsoDelCFDI>
                                                <xsl:value-of
                                                  select="//customer/paymentConfig/payment/cfdiUse/id"
                                                />
                                            </org:UsoDelCFDI>
                                        </xsl:if>
                                        <xsl:if
                                            test="//customer/paymentConfig/payment/paymentDeadline != ''">
                                            <org:PlazoDePago_c>
                                                <xsl:value-of
                                                  select="//customer/paymentConfig/payment/paymentDeadline/name"
                                                />
                                            </org:PlazoDePago_c>
                                        </xsl:if>
                                        <xsl:if
                                            test="//customer/paymentConfig/payment/paymentWay != ''">
                                            <org:FormaDePago_c>
                                                <xsl:value-of
                                                  select="//customer/paymentConfig/payment/paymentWay/id"
                                                />
                                            </org:FormaDePago_c>
                                        </xsl:if>
                                    
                                        <xsl:if
                                            test="//customer/locations/location/address/population">
                                            <xsl:if test="//customer/locations/location/address/population/id">
                                                <org:IDPoblacion_LD_c>
                                                    <xsl:value-of
                                                        select="xs:string(//customer/locations/location/address/population/id)"
                                                    />
                                                </org:IDPoblacion_LD_c>
                                            </xsl:if>
                                           
                                           
                                        </xsl:if>
                                        <org:Poblacion_LD_c>
                                            <xsl:value-of select="//customer/locations/location/address/population/name"/>
                                        </org:Poblacion_LD_c>
                                        <xsl:if
                                            test="//customer/locations/location/address/locality">
                                            <xsl:if test="//customer/locations/location/address/locality/id">
                                                <org:IDLocalidad_c>
                                                    <xsl:value-of select="xs:string(//customer/locations/location/address/locality/id)"/>
                                                </org:IDLocalidad_c>
                                            </xsl:if>
                                           
                                           
                                        </xsl:if>
                                        <org:Localidad_LD_c>
                                            <xsl:value-of select="//customer/locations/location/address/locality/name"/>
                                        </org:Localidad_LD_c>
                                        <xsl:if
                                            test="//customer/locations/location/address/municipality">
                                            <xsl:if test="//customer/locations/location/address/municipality/id">
                                                <org:IDMunicipio_LD_c>
                                                    <xsl:value-of select="xs:string(//customer/locations/location/address/municipality/id)"/>
                                                </org:IDMunicipio_LD_c>
                                            </xsl:if>
                                            
                                            
                                           
                                        </xsl:if>
                                        <org:Municipio_LD_c>
                                            <xsl:value-of select="//customer/locations/location/address/municipality/name"/>
                                        </org:Municipio_LD_c>
                                    </org:OrganizationProfile>
                                </sal:OrganizationParty>
                            </typ:salesParty>
                        </typ:updateSalesParty>
                    </soapenv:Body>
                </soapenv:Envelope>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="message">
        <messages>
            <result>Error</result>
            <xsl:if test="//customer/id = '' or not(//message/customer/id)">
                <message>
                    <id/>
                    <code>ATMN</code>
                    <description>Atributo [id customer] mandataorio Null o no enviado </description>
                </message>
            </xsl:if>
        </messages>
    </xsl:template>

    <xsl:template name="fecha">
        <xsl:param name="fech"/>
        <xsl:variable name="date" select="replace($fech, '\s', '#')"/>
        <xsl:variable name="dat" select="substring-before($date, '#')"/>
        <xsl:variable name="tmp" select="substring-after($date, '#')"/>
        <xsl:variable name="year" select="substring($dat, 7)"/>
        <xsl:variable name="mes" select="substring($dat, 4, 2)"/>
        <xsl:variable name="dia" select="substring($dat, 0, 3)"/>
        <xsl:variable name="newDate" select="concat($year, '-', $mes, '-', $dia)"/>
        <xsl:variable name="timeStamp" select="dateTime(xs:date($newDate), xs:time($tmp))"/>
        <xsl:variable name="newFecha" select="$timeStamp + xs:dayTimeDuration('PT6H')"/>
        <xsl:variable name="fecha2" as="xs:string"
            select="format-dateTime($newFecha, '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01].[f001]Z')"/>
        <xsl:value-of select="$fecha2"/>
    </xsl:template>
</xsl:stylesheet>
