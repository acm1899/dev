<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <xsl:if
            test="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:ConsolidationType = 'STANDARD'">
            <bill xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4"
                xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4">

                <id>
                    <xsl:attribute name="appCode">
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:DomainName"
                        />
                    </xsl:attribute>
                    <xsl:value-of
                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:Xid"
                    />
                </id>
                <creationDate>
                    <xsl:value-of
                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/otm:Payment/*:PaymentHeader/*:InvoiceDate/*:GLogDate"
                    />
                </creationDate>
                <approvedDate/>
                <approvedBy/>
                <capturedBy/>
                <consolidationType>
                    <xsl:value-of
                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:ConsolidationType"
                    />
                </consolidationType>
                <travel>
                    <id type="" appCode="">
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:ShipmentHeader/*:ShipmentGid/*:Gid/*:Xid"
                        />
                    </id>
                    <startDate/>
                    <endDate/>
                    <locations>
                        <location type="Exit">
                            <id type="B" typeName="Billing" appCode="MASTER/TRATE"/>
                            <description/>
                            <transactionCode/>
                            <status>
                                <id/>
                                <name/>
                            </status>
                            <isTemporary/>
                            <isTemplate/>
                            <rol>
                                <id/>
                                <description/>
                            </rol>
                            <address>
                                <id/>
                                <street/>
                                <interiorNumber/>
                                <externalNumber/>
                                <postalCode/>
                                <municipality/>
                                <population/>
                                <neighborhood/>
                                <locality/>
                                <entity/>
                                <state>
                                    <id/>
                                    <name/>
                                </state>
                                <city>
                                    <id/>
                                    <name/>
                                </city>
                                <country>
                                    <id/>
                                    <name/>
                                    <code/>
                                </country>
                                <timeZone id=""/>
                                <latitude/>
                                <allAddress/>
                                <length/>
                            </address>
                        </location>
                        <location type="Arrival">
                            <id type="B" typeName="Billing" appCode="MASTER/TRATE"/>
                            <description/>
                            <transactionCode/>
                            <status>
                                <id/>
                                <name/>
                            </status>
                            <isTemporary/>
                            <isTemplate/>
                            <rol>
                                <id/>
                                <description/>
                            </rol>
                            <address>
                                <id/>
                                <street/>
                                <interiorNumber/>
                                <externalNumber/>
                                <postalCode/>
                                <municipality/>
                                <population/>
                                <neighborhood/>
                                <locality/>
                                <entity/>
                                <state>
                                    <id/>
                                    <name/>
                                </state>
                                <city>
                                    <id/>
                                    <name/>
                                </city>
                                <country>
                                    <id/>
                                    <name/>
                                    <code/>
                                </country>
                                <timeZone id=""/>
                                <latitude/>
                                <allAddress/>
                                <length/>
                            </address>
                        </location>
                    </locations>
                    <stops>
                        <Stop>
                            <nameStop/>
                            <numberStop/>
                        </Stop>
                    </stops>
                    <hours/>
                    <transportation>
                        <truck>
                            <id type="Truck" typeName="Torton"/>
                            <mileage/>
                            <enrollment/>
                            <value/>
                            <status id="12" appCode="MASTER/TRATE">
                                <id/>
                                <type/>
                                <name/>
                                <value/>
                            </status>
                            <equipments>
                                <Equipment>
                                    <id type="Dollys"/>
                                    <code/>
                                    <name/>
                                    <cost/>
                                    <status/>
                                </Equipment>
                            </equipments>
                        </truck>
                    </transportation>
                </travel>
                <cedis>
                    <id appCode="MASTER" name=""/>
                    <name/>
                    <stateCode/>
                </cedis>
                <drivers>
                    <driver type="Pricipal">
                        <id/>
                        <name/>
                        <status/>
                        <pay type="MASTER.PAGO_CONDUCTOR"/>
                        <license>
                            <folio/>
                            <expiration/>
                            <status/>
                        </license>
                        <bankAccount>
                            <id/>
                            <name/>
                            <numberAccount/>
                            <interbankAccount/>
                        </bankAccount>
                    </driver>
                    <driver type="Secondary">
                        <id/>
                        <name/>
                        <status/>
                        <pay type="MASTER.PAGO_MANIOBRISTA"/>
                        <license>
                            <folio/>
                            <expiration/>
                            <status/>
                        </license>
                        <count>
                            <bankAccount>
                                <id/>
                                <name/>
                                <numberAccount/>
                                <interbankAccount/>
                            </bankAccount>
                        </count>
                    </driver>
                </drivers>
                <folio/>
                <lineItems>
                    <xsl:for-each
                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                        <xsl:variable name="line" select="position()"/>
                        <lineItem>
                            <sequence>
                                <xsl:value-of
                                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:AssignedNum"
                                />
                            </sequence>
                            <typeCost></typeCost>
                            <accesory>
                                <id type=""></id>
                                <name></name>
                                <description></description>
                                <code>
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                    />
                                </code>
                                <cost>
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                    />
                                </cost>
                                <generalLedger>
                                    <id/>
                                    <name/>
                                    <code>
                                        <xsl:value-of
                                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:GeneralLedgerGid/*:Gid/*:Xid"
                                        />
                                    </code>
                                    <description/>
                                </generalLedger>
                            </accesory>
                        </lineItem>
                    </xsl:for-each>
                </lineItems>
                <customer>
                    <recordControl>
                        <creationDate/>
                        <modificationDate/>
                        <masterCreationDate/>
                        <masterModificationDate/>
                    </recordControl>
                    <masterID/>
                    <id type="company" appCode="MASTER_OCCIDENTE"/>
                    <transactionCode>NP</transactionCode>
                    <!--   **datosOTM   -->
                    <lastName/>
                    <firstName/>
                    <!--   **datosOTM, usado como CorporationName en OTM   -->
                    <businessName/>
                    <fiscalName/>
                    <!--   **razon social usada para locationName en OTM   -->
                    <clasificationID/>
                    <thirdCustomerType/>
                    <rfc>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/otm:Location[2]/*:Corporation/*:CorporationName"
                        />
                    </rfc>
                    <description/>
                    <type id="01"/>
                    <siteURL/>
                    <salesAgent>
                        <id/>
                        <name/>
                    </salesAgent>
                    <status>
                        <id/>
                        <name/>
                    </status>
                    <contacts>
                        <contact>
                            <id type="Principal" appCode="MASTER.OCCIDENTE"/>
                            <firstContact/>
                            <name/>
                            <phone/>
                            <status id="12" appCode="MASTER.OCCIDENTE">
                                <id/>
                                <type/>
                                <name/>
                                <value/>
                            </status>
                            <officePhone/>
                            <isNotification/>
                            <comunicationMethod/>
                            <!--   **datosOTM   -->
                            <codeMethod/>
                            <!--   **datosOTM, constante   -->
                            <mail/>
                            <fax/>
                            <!--   **datosOTM   -->
                            <language>
                                <!--   **datosOTM   -->
                                <id/>
                                <!--   **datosOTM   -->
                                <name/>
                                <!--   **datosOTM   -->
                            </language>
                            <!--   **datosOTM   -->
                        </contact>
                    </contacts>
                    <comments>
                        <!--   **datosOTM   -->
                        <id/>
                        <!--   **datosOTM   -->
                        <text/>
                        <!--   **datosOTM   -->
                    </comments>
                    <!--   **datosOTM   -->
                    <locations>
                        <location>
                            <id type="B" typeName="Billing" appCode="MASTER/TRATE"/>
                            <description/>
                            <transactionCode>NP</transactionCode>
                            <!--   **datosOTM   -->
                            <status>
                                <id/>
                                <name/>
                            </status>
                            <isTemporary/>
                            <!--   **datosOTM   -->
                            <isTemplate/>
                            <!--   **datosOTM   -->
                            <rol>
                                <id>1</id>
                                <description/>
                            </rol>
                            <address>
                                <id/>
                                <street/>
                                <interiorNumber/>
                                <externalNumber/>
                                <postalCode/>
                                <municipality>
                                    <id/>
                                    <name/>
                                    <code/>
                                </municipality>
                                <population>
                                    <id/>
                                    <name/>
                                    <code/>
                                </population>
                                <neighborhood/>
                                <locality>
                                    <id/>
                                    <name/>
                                    <code/>
                                </locality>
                                <entity/>
                                <state>
                                    <id/>
                                    <name/>
                                    <code/>
                                </state>
                                <city>
                                    <id/>
                                    <name/>
                                </city>
                                <country>
                                    <id/>
                                    <name/>
                                    <code>MX</code>
                                    <!--   **datosOTM, constante   -->
                                </country>
                                <timeZone id="467"/>
                                <!--   **datosOTM   -->
                                <latitude/>
                                <!--   **datosOTM   -->
                                <length> </length>
                                <!--   **datosOTM   -->
                                <allAddress/>
                            </address>
                            <contacts>
                                <contact>
                                    <id type="Principal" appCode="MASTER.OCCIDENTE"/>
                                    <firstContact/>
                                    <name/>
                                    <phone/>
                                    <status id="12" appCode="MASTER/TRATE">
                                        <id/>
                                        <type/>
                                        <name/>
                                        <value/>
                                    </status>
                                    <officePhone/>
                                    <isNotification/>
                                    <comunicationMethod/>
                                    <!--   **datosOTM   -->
                                    <codeMethod1/>
                                    <!--   **datosOTM, constante   -->
                                    <mail>jose.sal@mail.com</mail>
                                    <fax/>
                                    <!--   **datosOTM   -->
                                    <language>
                                        <!--   **datosOTM   -->
                                        <id>89</id>
                                        <!--   **datosOTM   -->
                                        <name>Spanish</name>
                                        <!--   **datosOTM   -->
                                    </language>
                                    <!--   **datosOTM   -->
                                </contact>
                            </contacts>
                        </location>
                    </locations>
                    <paymentConfig>
                        <paymentWay>
                            <id/>
                            <name/>
                        </paymentWay>
                        <paymentMethod>
                            <id/>
                            <name/>
                        </paymentMethod>
                        <cfdiUse>
                            <id/>
                            <name/>
                        </cfdiUse>
                        <paymentDeadline>
                            <id/>
                            <name/>
                        </paymentDeadline>
                    </paymentConfig>
                </customer>
                <currency>
                    <id>1</id>
                    <name>Peso Mexicano</name>
                    <code>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:GlobalCurrencyCode"
                        />
                    </code>
                    <value>0.052</value>
                </currency>
                <paymentConfig>
                    <folio/>
                    <amountOfPay>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:MonetaryAmount"
                        />
                    </amountOfPay>
                    <paymentWay>
                        <id/>
                        <name/>
                    </paymentWay>
                    <paymentMethod>
                        <id/>
                        <name/>
                    </paymentMethod>
                    <cfdiUse>
                        <id/>
                        <name/>
                    </cfdiUse>
                    <paymentDeadline>
                        <id/>
                        <name/>
                        <value/>
                    </paymentDeadline>
                </paymentConfig>

            </bill>
        </xsl:if>
        <xsl:if
            test="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:ConsolidationType = 'PARENT'">
            <bill xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4"
                xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4">
                <id>
                    <xsl:attribute name="appCode">
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:DomainName"
                        />
                    </xsl:attribute>
                    <xsl:value-of
                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:Xid"
                    />
                </id>
                <creationDate>
                    <xsl:value-of
                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/otm:Payment/*:PaymentHeader/*:InvoiceDate/*:GLogDate"
                    />
                </creationDate>
                <approvedDate/>
                <approvedBy/>
                <capturedBy/>
                <consolidationType>
                    <xsl:value-of
                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:ConsolidationType"
                    />
                </consolidationType>
                <travel>
                    <id type="" appCode="">
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:ShipmentHeader/*:ShipmentGid/*:Gid/*:Xid"
                        />
                    </id>
                    <startDate/>
                    <endDate/>
                    <locations>
                        <location type="Exit">
                            <id type="B" typeName="Billing" appCode="MASTER/TRATE"/>
                            <description/>
                            <transactionCode/>
                            <status>
                                <id/>
                                <name/>
                            </status>
                            <isTemporary/>
                            <isTemplate/>
                            <rol>
                                <id/>
                                <description/>
                            </rol>
                            <address>
                                <id/>
                                <street/>
                                <interiorNumber/>
                                <externalNumber/>
                                <postalCode/>
                                <municipality/>
                                <population/>
                                <neighborhood/>
                                <locality/>
                                <entity/>
                                <state>
                                    <id/>
                                    <name/>
                                </state>
                                <city>
                                    <id/>
                                    <name/>
                                </city>
                                <country>
                                    <id/>
                                    <name/>
                                    <code/>
                                </country>
                                <timeZone id=""/>
                                <latitude/>
                                <allAddress/>
                                <length/>
                            </address>
                        </location>
                        <location type="Arrival">
                            <id type="B" typeName="Billing" appCode="MASTER/TRATE"/>
                            <description/>
                            <transactionCode/>
                            <status>
                                <id/>
                                <name/>
                            </status>
                            <isTemporary/>
                            <isTemplate/>
                            <rol>
                                <id/>
                                <description/>
                            </rol>
                            <address>
                                <id/>
                                <street/>
                                <interiorNumber/>
                                <externalNumber/>
                                <postalCode/>
                                <municipality/>
                                <population/>
                                <neighborhood/>
                                <locality/>
                                <entity/>
                                <state>
                                    <id/>
                                    <name/>
                                </state>
                                <city>
                                    <id/>
                                    <name/>
                                </city>
                                <country>
                                    <id/>
                                    <name/>
                                    <code/>
                                </country>
                                <timeZone id=""/>
                                <latitude/>
                                <allAddress/>
                                <length/>
                            </address>
                        </location>
                    </locations>
                    <stops>
                        <Stop>
                            <nameStop/>
                            <numberStop/>
                        </Stop>
                    </stops>
                    <hours/>
                    <transportation>
                        <truck>
                            <id type="Truck" typeName="Torton"/>
                            <mileage/>
                            <enrollment/>
                            <value/>
                            <status id="12" appCode="MASTER/TRATE">
                                <id/>
                                <type/>
                                <name/>
                                <value/>
                            </status>
                            <equipments>
                                <Equipment>
                                    <id type="Dollys"/>
                                    <code/>
                                    <name/>
                                    <cost/>
                                    <status/>
                                </Equipment>
                            </equipments>
                        </truck>
                    </transportation>
                </travel>
                <cedis>
                    <id appCode="MASTER" name=""/>
                    <name/>
                    <stateCode/>
                </cedis>
                <drivers>
                    <driver type="Pricipal">
                        <id/>
                        <name/>
                        <status/>
                        <pay type="MASTER.PAGO_CONDUCTOR"/>
                        <license>
                            <folio/>
                            <expiration/>
                            <status/>
                        </license>
                        <bankAccount>
                            <id/>
                            <name/>
                            <numberAccount/>
                            <interbankAccount/>
                        </bankAccount>
                    </driver>
                    <driver type="Secondary">
                        <id/>
                        <name/>
                        <status/>
                        <pay type="MASTER.PAGO_MANIOBRISTA"/>
                        <license>
                            <folio/>
                            <expiration/>
                            <status/>
                        </license>
                        <count>
                            <bankAccount>
                                <id/>
                                <name/>
                                <numberAccount/>
                                <interbankAccount/>
                            </bankAccount>
                        </count>
                    </driver>
                </drivers>
                <folio/>
                <lineItems>
                    <xsl:for-each
                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                        <xsl:variable name="line" select="position()"/>
                        <lineItem>
                            <sequence>
                                <xsl:value-of
                                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:AssignedNum"
                                />
                            </sequence>
                            <typeCost>base</typeCost>
                            <accesory>
                                <id type="Gasolina">1</id>
                                <name>Diesel</name>
                                <description>Gasolina Adicional</description>
                                <code>
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                    />
                                </code>
                                <cost>
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                    />
                                </cost>
                                <generalLedger>
                                    <id/>
                                    <name/>
                                    <code>
                                        <xsl:value-of
                                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:GeneralLedgerGid/*:Gid/*:Xid"
                                        />
                                    </code>
                                    <description/>
                                </generalLedger>
                            </accesory>
                        </lineItem>
                    </xsl:for-each>
                </lineItems>
                <customer>
                    <recordControl>
                        <creationDate/>
                        <modificationDate/>
                        <masterCreationDate/>
                        <masterModificationDate/>
                    </recordControl>
                    <masterID/>
                    <id type="company" appCode="MASTER_OCCIDENTE"/>
                    <transactionCode>NP</transactionCode>
                    <!--   **datosOTM   -->
                    <lastName/>
                    <firstName/>
                    <!--   **datosOTM, usado como CorporationName en OTM   -->
                    <businessName/>
                    <fiscalName/>
                    <!--   **razon social usada para locationName en OTM   -->
                    <clasificationID/>
                    <thirdCustomerType/>
                    <rfc>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/otm:Location[2]/*:Corporation/*:CorporationName"
                        />
                    </rfc>
                    <description/>
                    <type id="01"/>
                    <siteURL/>
                    <salesAgent>
                        <id/>
                        <name/>
                    </salesAgent>
                    <status>
                        <id/>
                        <name/>
                    </status>
                    <contacts>
                        <contact>
                            <id type="Principal" appCode="MASTER.OCCIDENTE"/>
                            <firstContact/>
                            <name/>
                            <phone/>
                            <status id="12" appCode="MASTER.OCCIDENTE">
                                <id/>
                                <type/>
                                <name/>
                                <value/>
                            </status>
                            <officePhone/>
                            <isNotification/>
                            <comunicationMethod/>
                            <!--   **datosOTM   -->
                            <codeMethod/>
                            <!--   **datosOTM, constante   -->
                            <mail/>
                            <fax/>
                            <!--   **datosOTM   -->
                            <language>
                                <!--   **datosOTM   -->
                                <id/>
                                <!--   **datosOTM   -->
                                <name/>
                                <!--   **datosOTM   -->
                            </language>
                            <!--   **datosOTM   -->
                        </contact>
                    </contacts>
                    <comments>
                        <!--   **datosOTM   -->
                        <id/>
                        <!--   **datosOTM   -->
                        <text/>
                        <!--   **datosOTM   -->
                    </comments>
                    <!--   **datosOTM   -->
                    <locations>
                        <location>
                            <id type="B" typeName="Billing" appCode="MASTER/TRATE"/>
                            <description/>
                            <transactionCode>NP</transactionCode>
                            <!--   **datosOTM   -->
                            <status>
                                <id/>
                                <name/>
                            </status>
                            <isTemporary/>
                            <!--   **datosOTM   -->
                            <isTemplate/>
                            <!--   **datosOTM   -->
                            <rol>
                                <id>1</id>
                                <description/>
                            </rol>
                            <address>
                                <id/>
                                <street/>
                                <interiorNumber/>
                                <externalNumber/>
                                <postalCode/>
                                <municipality>
                                    <id/>
                                    <name/>
                                    <code/>
                                </municipality>
                                <population>
                                    <id/>
                                    <name/>
                                    <code/>
                                </population>
                                <neighborhood/>
                                <locality>
                                    <id/>
                                    <name/>
                                    <code/>
                                </locality>
                                <entity/>
                                <state>
                                    <id/>
                                    <name/>
                                    <code/>
                                </state>
                                <city>
                                    <id/>
                                    <name/>
                                </city>
                                <country>
                                    <id/>
                                    <name/>
                                    <code>MX</code>
                                    <!--   **datosOTM, constante   -->
                                </country>
                                <timeZone id="467"/>
                                <!--   **datosOTM   -->
                                <latitude/>
                                <!--   **datosOTM   -->
                                <length> </length>
                                <!--   **datosOTM   -->
                                <allAddress/>
                            </address>
                            <contacts>
                                <contact>
                                    <id type="Principal" appCode="MASTER.OCCIDENTE"/>
                                    <firstContact/>
                                    <name/>
                                    <phone/>
                                    <status id="12" appCode="MASTER/TRATE">
                                        <id/>
                                        <type/>
                                        <name/>
                                        <value/>
                                    </status>
                                    <officePhone/>
                                    <isNotification/>
                                    <comunicationMethod/>
                                    <!--   **datosOTM   -->
                                    <codeMethod1/>
                                    <!--   **datosOTM, constante   -->
                                    <mail/>
                                    <fax/>
                                    <!--   **datosOTM   -->
                                    <language>
                                        <!--   **datosOTM   -->
                                        <id>89</id>
                                        <!--   **datosOTM   -->
                                        <name>Spanish</name>
                                        <!--   **datosOTM   -->
                                    </language>
                                    <!--   **datosOTM   -->
                                </contact>
                            </contacts>
                        </location>
                    </locations>
                    <paymentConfig>
                        <paymentWay>
                            <id/>
                            <name/>
                        </paymentWay>
                        <paymentMethod>
                            <id/>
                            <name/>
                        </paymentMethod>
                        <cfdiUse>
                            <id/>
                            <name/>
                        </cfdiUse>
                        <paymentDeadline>
                            <id/>
                            <name/>
                        </paymentDeadline>
                    </paymentConfig>
                </customer>
                <currency>
                    <id/>
                    <name/>
                    <code/>
                    <value/>
                </currency>
                <paymentConfig>
                    <folio/>
                    <amountOfPay>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:MonetaryAmount"
                        />
                    </amountOfPay>
                    <paymentWay>
                        <id/>
                        <name/>
                    </paymentWay>
                    <paymentMethod>
                        <id/>
                        <name/>
                    </paymentMethod>
                    <cfdiUse>
                        <id/>
                        <name/>
                    </cfdiUse>
                    <paymentDeadline>
                        <id/>
                        <name/>
                        <value/>
                    </paymentDeadline>
                </paymentConfig>
                <childrenBill>
                    <xsl:for-each
                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing">
                        <xsl:variable name="pos" select="position()"/>
                        <bill>
                            <id>
                                <xsl:attribute name="appCode">
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:DomainName"
                                    />
                                </xsl:attribute>
                                <xsl:value-of
                                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:Xid"
                                />
                            </id>
                            <creationDate>
                                <xsl:value-of
                                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceDate/*:GLogDate"
                                />
                            </creationDate>
                            <approvedDate/>
                            <approvedBy/>
                            <capturedBy/>
                            <consolidationType>
                                <!--
<xsl:value-of
                                select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:ConsolidationType"
                            />
-->
                                <xsl:value-of
                                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentHeader/*:ConsolidationType"
                                />
                            </consolidationType>
                            <travel>
                                <id type="" appCode="">
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Shipment/*:ShipmentHeader/*:ShipmentGid/*:Gid/*:Xid"
                                    />
                                </id>
                                <startDate/>
                                <endDate/>
                                <locations>
                                    <location type="Exit">
                                        <id type="B" typeName="Billing" appCode="MASTER/TRATE"/>
                                        <description/>
                                        <transactionCode/>
                                        <status>
                                            <id/>
                                            <name/>
                                        </status>
                                        <isTemporary/>
                                        <isTemplate/>
                                        <rol>
                                            <id/>
                                            <description/>
                                        </rol>
                                        <address>
                                            <id/>
                                            <street/>
                                            <interiorNumber/>
                                            <externalNumber/>
                                            <postalCode/>
                                            <municipality/>
                                            <population/>
                                            <neighborhood/>
                                            <locality/>
                                            <entity/>
                                            <state>
                                                <id/>
                                                <name/>
                                            </state>
                                            <city>
                                                <id/>
                                                <name/>
                                            </city>
                                            <country>
                                                <id/>
                                                <name/>
                                                <code/>
                                            </country>
                                            <timeZone id=""/>
                                            <latitude/>
                                            <allAddress/>
                                            <length/>
                                        </address>
                                    </location>
                                    <location type="Arrival">
                                        <id type="B" typeName="Billing" appCode="MASTER/TRATE"/>
                                        <description/>
                                        <transactionCode/>
                                        <status>
                                            <id/>
                                            <name/>
                                        </status>
                                        <isTemporary/>
                                        <isTemplate/>
                                        <rol>
                                            <id/>
                                            <description/>
                                        </rol>
                                        <address>
                                            <id/>
                                            <street/>
                                            <interiorNumber/>
                                            <externalNumber/>
                                            <postalCode/>
                                            <municipality/>
                                            <population/>
                                            <neighborhood/>
                                            <locality/>
                                            <entity/>
                                            <state>
                                                <id/>
                                                <name/>
                                            </state>
                                            <city>
                                                <id/>
                                                <name/>
                                            </city>
                                            <country>
                                                <id/>
                                                <name/>
                                                <code/>
                                            </country>
                                            <timeZone id=""/>
                                            <latitude/>
                                            <allAddress/>
                                            <length/>
                                        </address>
                                    </location>
                                </locations>
                                <stops>
                                    <Stop>
                                        <nameStop/>
                                        <numberStop/>
                                    </Stop>
                                </stops>
                                <hours/>
                                <transportation>
                                    <truck>
                                        <id type="Truck" typeName="Torton"/>
                                        <mileage/>
                                        <enrollment/>
                                        <value/>
                                        <status id="12" appCode="MASTER/TRATE">
                                            <id/>
                                            <type/>
                                            <name/>
                                            <value/>
                                        </status>
                                        <equipments>
                                            <Equipment>
                                                <id type="Dollys"/>
                                                <code/>
                                                <name/>
                                                <cost/>
                                                <status/>
                                            </Equipment>
                                        </equipments>
                                    </truck>
                                </transportation>
                            </travel>
                            <lineItems>
                                <xsl:for-each
                                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                                    <xsl:variable name="num" select="position()"/>
                                    <lineItem>
                                        <sequence>
                                            <xsl:value-of
                                                select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$num]/*:AssignedNum"
                                            />
                                        </sequence>
                                        <accesory>
                                            <id/>
                                            <name/>
                                            <description/>
                                            <code>
                                                <xsl:value-of
                                                  select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$num]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                                />
                                            </code>
                                            <cost>
                                                <xsl:value-of
                                                  select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$num]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                                />
                                            </cost>
                                            <generalLedger>
                                                <id/>
                                                <name/>
                                                <code>
                                                  <xsl:value-of
                                                  select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail[$num]/*:GenericLineItem/*:CommonInvoiceLineElements/*:GeneralLedgerGid/*:Gid/*:Xid"
                                                  />
                                                </code>
                                                <description/>
                                            </generalLedger>
                                        </accesory>
                                    </lineItem>
                                </xsl:for-each>
                                <!--
 <lineItem>
                                <xsl:variable name="num" select="position()"/>
                                <sequence>
                                    <xsl:value-of select="$num"/>
                                </sequence>
                                <typeCost>base</typeCost>
                                <accesory>
                                    <id type="Gasolina">1</id>
                                    <name>Diesel</name>
                                    <description>Gasolina Adicional</description>
                                    <code>
                                        <xsl:value-of
                                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                        />
                                    </code>
                                    <cost>
                                        <xsl:value-of
                                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                        />
                                    </cost>
                                    <generalLedger>
                                        <id/>
                                        <name/>
                                        <code>
                                            <xsl:value-of
                                                select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem/*:CommonInvoiceLineElements/*:GeneralLedgerGid/*:Gid/*:Xid"
                                            />
                                        </code>
                                        <description/>
                                    </generalLedger>
                                </accesory>
                            </lineItem>
-->
                            </lineItems>
                            <customer>
                                <rfc>
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Shipment/*:Location[2]/*:Corporation/*:CorporationName"
                                    />
                                </rfc>
                            </customer>
                            <currency>
                                <id>1</id>
                                <name>Peso Mexicano</name>
                                <code>
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:GlobalCurrencyCode"
                                    />
                                </code>
                                <value>0.052</value>
                            </currency>
                            <paymentConfig>
                                <folio/>
                                <amountOfPay>
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:MonetaryAmount"
                                    />
                                </amountOfPay>
                                <paymentWay>
                                    <id/>
                                    <name/>
                                </paymentWay>
                                <paymentMethod>
                                    <id/>
                                    <name/>
                                </paymentMethod>
                                <cfdiUse>
                                    <id/>
                                    <name/>
                                </cfdiUse>
                                <paymentDeadline>
                                    <id/>
                                    <name/>
                                    <value/>
                                </paymentDeadline>
                            </paymentConfig>
                        </bill>
                    </xsl:for-each>
                </childrenBill>
            </bill>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
