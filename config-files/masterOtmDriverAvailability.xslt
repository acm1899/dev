<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="/message/availableDriver/drivers/driver/id = '' or not(/message/availableDriver/drivers/driver/id)">
                <xsl:apply-templates select="message"/>
            </xsl:when>
            <xsl:when test="/message/availableDriver/drivers/driver/id/@type = '' or not(/message/availableDriver/drivers/driver/id/@type)">
                <xsl:apply-templates select="message"/>
            </xsl:when>
            <xsl:when test="/message/availableDriver/drivers/driver/firstName = '' or not(/message/availableDriver/drivers/driver/firstName)">
                <xsl:apply-templates select="message"/>
            </xsl:when>
            <xsl:when test="/message/availableDriver/drivers/driver/lastName = '' or not(/message/availableDriver/drivers/driver/lastName)">
                <xsl:apply-templates select="message"/>
            </xsl:when>
            <xsl:otherwise>
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                    xmlns:tran="http://xmlns.oracle.com/apps/otm/TransmissionService"
                    xmlns:v6="http://xmlns.oracle.com/apps/otm/transmission/v6.4">
                    <soapenv:Header>
                        <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                            <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                                <wsse:Username>MASTER.IVAN_ROSALES</wsse:Username>
                                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">Sol140218357</wsse:Password>
                            </wsse:UsernameToken>
                        </wsse:Security>
                    </soapenv:Header>
                    <soapenv:Body>
                        <tran:publish>
                            <Transmission xmlns="http://xmlns.oracle.com/apps/otm/transmission/v6.4">
                                <TransmissionHeader/>
                                <TransmissionBody>
                                    <GLogXMLElement>
                                        <otm:Driver xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4"
                                            xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4">
                                            <otm:DriverGid>
                                                <otm:Gid>
                                                    <otm:DomainName>MASTER</otm:DomainName>
                                                    <otm:Xid>
                                                        <xsl:value-of select="/message/availableDriver/drivers/driver/id"/>
                                                    </otm:Xid>
                                                </otm:Gid>
                                            </otm:DriverGid>
                                            <otm:TransactionCode/>
                                            <otm:DriverHeader>
                                                <otm:FirstName>
                                                    <xsl:value-of select="/message/availableDriver/drivers/driver/firstName"/>
                                                </otm:FirstName>
                                                <otm:LastName>
                                                    <xsl:value-of select="/message/availableDriver/drivers/driver/lastName"/>
                                                </otm:LastName>
                                                <otm:AdjHireDate>
                                                    <xsl:choose>
                                                        <xsl:when test="/message/availableDriver/drivers/driver/dateAdmission = '' or not(/message/availableDriver/drivers/driver/dateAdmission)">
                                                            <otm:GLogDate/>
                                                            <otm:TZId/>
                                                            <otm:TZOffset/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:variable name="dateMaster" select="/message/availableDriver/drivers/driver/dateAdmission"/>
                                                            
                                                            <xsl:variable name="date" select="replace($dateMaster, '[/,\s,:]', '')"/>
                                                            <xsl:variable name="year" select="substring($date, 5, 4)"/>
                                                            <xsl:variable name="mes" select="substring($date, 3, 2)"/>
                                                            <xsl:variable name="day" select="substring($date, 0, 3)"/>
                                                            <xsl:variable name="hour" select="substring($date, 9, 2)"/>
                                                            <xsl:variable name="min" select="substring($date, 11, 2)"/>
                                                            <xsl:variable name="sec" select="substring($date, 13, 2)"/>
                                                            <xsl:variable name="newDate" select="concat($year, '-', $mes, '-', $day)"/>
                                                            <xsl:variable name="newTime" select="concat($hour, ':', $min, ':', $sec)"/>
                                                            <xsl:variable name="ddtt" select="dateTime(xs:date($newDate), xs:time($newTime))"/>
                                                            <xsl:variable name="dateOtm" select="format-dateTime($ddtt, '[Y0001][M01][D01][H01][m01][s01]')"/>
                                                            
                                                            <otm:GLogDate><xsl:value-of select="$dateOtm"/></otm:GLogDate>
                                                            <otm:TZId>UTC</otm:TZId>
                                                            <otm:TZOffset>+00:00</otm:TZOffset>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </otm:AdjHireDate>
                                                <otm:DriverTypeGid>
                                                    <otm:Gid>
                                                        <otm:DomainName>MASTER</otm:DomainName>
                                                        <otm:Xid>
                                                            <xsl:value-of select="/message/availableDriver/drivers/driver/id/@type"/>
                                                        </otm:Xid>
                                                    </otm:Gid>
                                                </otm:DriverTypeGid>
                                                <otm:ResourceType/>
                                                <otm:UseHosHistory>
                                                    <xsl:value-of select="/message/availableDriver/drivers/driver/hosHistory"/>
                                                </otm:UseHosHistory>
                                                <otm:IsActive>
                                                    <xsl:value-of select="/message/availableDriver/drivers/driver/status/id"/>
                                                </otm:IsActive>
                                            </otm:DriverHeader>
                                            <otm:Status>
                                                <otm:StatusTypeGid>
                                                    <otm:Gid>
                                                        <otm:DomainName>MASTER</otm:DomainName>
                                                        <otm:Xid>
                                                            <xsl:variable name="statusType" select="/message/availableDriver/drivers/driver/status/type"/>
                                                            <xsl:value-of select="substring-after($statusType, '.')"/>
                                                        </otm:Xid>
                                                    </otm:Gid>
                                                </otm:StatusTypeGid>
                                                <otm:StatusValueGid>
                                                    <otm:Gid>
                                                        <otm:DomainName>MASTER</otm:DomainName>
                                                        <otm:Xid>
                                                            <xsl:value-of select="/message/availableDriver/drivers/driver/status/value"/>
                                                        </otm:Xid>
                                                    </otm:Gid>
                                                </otm:StatusValueGid>
                                            </otm:Status>
                                            <otm:DriverCdl>
                                                <otm:TransactionCode/>
                                                <otm:CdlClass>
                                                    <xsl:value-of select="/message/availableDriver/drivers/driver/license/type"/>
                                                </otm:CdlClass>
                                                <otm:CdlNum>
                                                    <xsl:value-of select="/message/availableDriver/drivers/driver/license/folio"/>
                                                </otm:CdlNum>
                                                <otm:CdlExpDate>
                                                    <xsl:choose>
                                                        <xsl:when test="/message/availableDriver/drivers/driver/license/folio = '' or not(/message/availableDriver/drivers/driver/license/folio)">
                                                            <otm:GLogDate></otm:GLogDate>
                                                            <otm:TZId></otm:TZId>
                                                            <otm:TZOffset></otm:TZOffset>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:variable name="dateExp" select="/message/availableDriver/drivers/driver/license/expiration"/>
                                                            
                                                            <xsl:variable name="date2" select="replace($dateExp, '[/,\s,:]', '')"/>
                                                            <xsl:variable name="year2" select="substring($date2, 5, 4)"/>
                                                            <xsl:variable name="mes2" select="substring($date2, 3, 2)"/>
                                                            <xsl:variable name="day2" select="substring($date2, 0, 3)"/>
                                                            <xsl:variable name="hour2" select="substring($date2, 9, 2)"/>
                                                            <xsl:variable name="min2" select="substring($date2, 11, 2)"/>
                                                            <xsl:variable name="sec2" select="substring($date2, 13, 2)"/>
                                                            <xsl:variable name="newDate2" select="concat($year2, '-', $mes2, '-', $day2)"/>
                                                            <xsl:variable name="newTime2" select="concat($hour2, ':', $min2, ':', $sec2)"/>
                                                            <xsl:variable name="ddtt2" select="dateTime(xs:date($newDate2), xs:time($newTime2))"/>
                                                            <xsl:variable name="dateOtm2" select="format-dateTime($ddtt2, '[Y0001][M01][D01][H01][m01][s01]')"/>
                                                            
                                                            <otm:GLogDate><xsl:value-of select="$dateOtm2"/></otm:GLogDate>
                                                            <otm:TZId>UTC</otm:TZId>
                                                            <otm:TZOffset>+00:00</otm:TZOffset>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </otm:CdlExpDate>
                                                <otm:CdlIssueCountryGid>
                                                    <otm:Gid>
                                                        <otm:Xid/>
                                                    </otm:Gid>
                                                </otm:CdlIssueCountryGid>
                                                <otm:CdlIssueState>
                                                    <xsl:value-of select="/message/availableDriver/drivers/driver/license/origin"/>
                                                </otm:CdlIssueState>
                                            </otm:DriverCdl>
                                            <otm:FlexFieldStrings>
                                                <otm:Attribute1/>
                                            </otm:FlexFieldStrings>
                                            <otm:FlexFieldNumbers>
                                                <otm:AttributeNumber2/>
                                                <otm:AttributeNumber3>
                                                    <xsl:value-of select="/message/availableDriver/drivers/driver/phone"/>
                                                </otm:AttributeNumber3>
                                                <otm:AttributeNumber4>
                                                    <xsl:value-of select="/message/availableDriver/drivers/driver/fixed_phone"/>
                                                </otm:AttributeNumber4>
                                            </otm:FlexFieldNumbers>
                                            <otm:FlexFieldDates/>
                                        </otm:Driver>
                                    </GLogXMLElement>
                                </TransmissionBody>
                            </Transmission>
                        </tran:publish>
                    </soapenv:Body>
                </soapenv:Envelope>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="message">
        <messages>
            <result>Error!</result>
            <xsl:if test="/message/availableDriver/drivers/driver/id = '' or not(/message/availableDriver/drivers/driver/id)">
                <message>
                    <id/>
                    <code>ATMN</code>
                    <description>Atributo [driver_id] mandatorio Null o No enviado</description>
                </message>
            </xsl:if>
            <xsl:if test="/message/availableDriver/drivers/driver/id/@type = '' or not(/message/availableDriver/drivers/driver/id/@type)">
                <message>
                    <id/>
                    <code>ATMN</code>
                    <description>Atributo [driver_type] mandatorio Null o No enviado</description>
                </message>
            </xsl:if>
            <xsl:if test="/message/availableDriver/drivers/driver/firstName = '' or not(/message/availableDriver/drivers/driver/firstName)">
                <message>
                    <id/>
                    <code>ATMN</code>
                    <description>Atributo [first_name] mandatorio Null o No enviado</description>
                </message>
            </xsl:if>
            <xsl:if test="/message/availableDriver/drivers/driver/lastName = '' or not(/message/availableDriver/drivers/driver/lastName)">
                <message>
                    <id/>
                    <code>ATMN</code>
                    <description>Atributo [last_name] mandatorio Null o No enviado</description>
                </message>
            </xsl:if>
        </messages>
    </xsl:template>
</xsl:stylesheet>