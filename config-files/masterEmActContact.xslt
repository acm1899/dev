<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        
        <!-- Fechas para actualizar -->
        <xsl:variable name="fecha" select="current-dateTime()"/>
        <xsl:variable name="fecha2"
            select="format-dateTime($fecha, '[Y0001]-[M01]-[D01]:T[H01]:[m01]:[s001]Z')"/>
        <xsl:variable name="newFecha" select="$fecha + xs:dayTimeDuration('PT5H')"/>
        <xsl:variable name="newFecha2" select="$newFecha + xs:dayTimeDuration('PT7H')"/>
        <xsl:variable name="fecha2"
            select="format-dateTime($newFecha, '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01].[f001]Z')"/>
        <xsl:variable name="fecha3"
            select="format-dateTime($newFecha2, '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01].[f001]Z')"/>
        <soapenv:Envelope xmlns:com="http://xmlns.oracle.com/apps/crmCommon/salesParties/commonService/" xmlns:con="http://xmlns.oracle.com/apps/crmCommon/salesParties/contactService/" xmlns:not="http://xmlns.oracle.com/apps/crmCommon/notes/noteService" xmlns:not1="http://xmlns.oracle.com/apps/crmCommon/notes/flex/noteDff/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://xmlns.oracle.com/apps/crmCommon/salesParties/contactService/types/">
            <soapenv:Header>
                <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                    <wsse:UsernameToken wsu:Id="UsernameToken-C74155B5951915653715834488306799">
                        <wsse:Username>Sergio.obregon@avansaas.com</wsse:Username>
                        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">Oracle1234</wsse:Password>
                        <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">ecIQIlDYiQiHGjLodMQmeA==</wsse:Nonce>
                        <wsu:Created>
                            <xsl:value-of select="$fecha2"/>
                        </wsu:Created>
                    </wsse:UsernameToken>
                    <wsu:Timestamp wsu:Id="TS-C74155B5951915653715834488275698">
                        <wsu:Created><xsl:value-of select="$fecha2"/></wsu:Created>
                        <wsu:Expires><xsl:value-of select="$fecha3"/></wsu:Expires>
                    </wsu:Timestamp>
                </wsse:Security>
            </soapenv:Header>
            <soapenv:Body>
                <typ:updateContact>
                    <typ:contact>
                        <con:PartyId>
                            <xsl:value-of select="//message/customer/contacts/contact/id"/>
                        </con:PartyId>
                        <xsl:if test="//message/customer/contacts/contact/firstName">
                            <con:FirstName>
                                <xsl:value-of select="//message/customer/contacts/contact/firstName"/>
                            </con:FirstName>
                        </xsl:if>
                        <xsl:if test="//message/customer/contacts/contact/lastName">
                            <con:LastName>
                                <xsl:value-of select="//message/customer/contacts/contact/lastName"/>
                            </con:LastName>
                        </xsl:if>
                        <xsl:if test="//message/customer/contacts/contact/lastName">
                            <con:LastName>
                                <xsl:value-of select="//message/customer/contacts/contact/lastName"/>
                            </con:LastName>
                        </xsl:if>
                        <xsl:if test="//message/customer/contacts/contact/countryCode">
                            <con:WorkPhoneCountryCode>
                                <xsl:value-of select="//message/customer/contacts/contact/countryCode"/>
                            </con:WorkPhoneCountryCode>
                        </xsl:if>
                        <xsl:if test="//message/customer/contacts/contact/areaCode">
                            <con:WorkPhoneAreaCode>
                                <xsl:value-of select="//message/customer/contacts/contact/areaCode"/>
                            </con:WorkPhoneAreaCode>
                        </xsl:if>
                        <xsl:if test="//message/customer/contacts/contact/ext">
                            <con:WorkPhoneExtension>
                                <xsl:value-of select="//message/customer/contacts/contact/ext"/>
                            </con:WorkPhoneExtension>
                        </xsl:if>
                        <xsl:if test="//message/customer/contacts/contact/phone">
                            <con:WorkPhoneNumber>
                                <xsl:value-of select="//message/customer/contacts/contact/phone"/>
                            </con:WorkPhoneNumber>
                        </xsl:if>
                        <xsl:if test="//message/customer/contacts/contact/firstContact">
                            <con:ContactIsPrimaryForAccount>
                                <xsl:value-of select="//message/customer/contacts/contact/firstContact"/>
                            </con:ContactIsPrimaryForAccount>
                        </xsl:if>
                        <xsl:if test="//message/customer/contacts/contact/mail">
                            <con:EmailAddress>
                                <xsl:value-of select="//message/customer/contacts/contact/mail"/>
                            </con:EmailAddress>
                        </xsl:if>
                       
                    </typ:contact>
                </typ:updateContact>
            </soapenv:Body>
        </soapenv:Envelope>
    </xsl:template>
</xsl:stylesheet>