<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" indent="yes"/>
    <!--  status  -->
    <xsl:key name="estatus" match="status/string" use="@id"/>
    <xsl:variable name="estE" select='document("estatusEquipo.xml")/status'/>
    
    <!--  Active  -->
    <xsl:key name="acti" match="activeEquipo/string" use="@id"/>
    <xsl:variable name="activeE" select='document("activeEquipo.xml")/activeEquipo'/>
    
    
    <xsl:template match="/">
        
        
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:tran="http://xmlns.oracle.com/apps/otm/TransmissionService"
            xmlns:v6="http://xmlns.oracle.com/apps/otm/transmission/v6.4">
            <soapenv:Header>
                <wsse:Security
                    xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                    <wsse:UsernameToken
                        xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                        <wsse:Username>MASTER.IVAN_ROSALES</wsse:Username>
                        <wsse:Password
                            Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"
                            >Sol140218357</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
            </soapenv:Header>
            <soapenv:Body>
                <tran:publish>
                    <Transmission xmlns="http://xmlns.oracle.com/apps/otm/transmission/v6.4">
                        <TransmissionHeader/>
                        <TransmissionBody>
                            <GLogXMLElement>
                                <otm:Equipment
                                    xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4"
                                    xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4">
                                    <otm:EquipmentGid>
                                        <otm:Gid>
                                            <otm:DomainName>
                                                <xsl:value-of
                                                  select="//availableEquipment/equipment/id/@appCode"
                                                />
                                            </otm:DomainName>
                                            <otm:Xid>
                                                <xsl:value-of
                                                  select="//availableEquipment/equipment/id"/>
                                            </otm:Xid>
                                        </otm:Gid>
                                    </otm:EquipmentGid>
                                    <otm:TransactionCode>IU</otm:TransactionCode>

                                    <otm:EquipmentTypeGid>
                                        <otm:Gid>
                                            <otm:DomainName>
                                                <xsl:value-of
                                                  select="//availableEquipment/equipment/id/@appCode"
                                                />
                                            </otm:DomainName>
                                            <otm:Xid>
                                                <xsl:value-of
                                                  select="//availableEquipment/equipment/id/@type"
                                                />
                                            </otm:Xid>
                                        </otm:Gid>
                                    </otm:EquipmentTypeGid>
                                    <otm:EquipmentOwner>
                                        <xsl:value-of select="//availableEquipment/cedis/name"/>
                                    </otm:EquipmentOwner>
                                    <otm:LicensePlate>
                                        <xsl:value-of
                                            select="//availableEquipment/equipment/code"/>
                                    </otm:LicensePlate>
                                    <otm:EquipmentTag1>
                                        <xsl:value-of
                                            select="//availableEquipment/equipment/iave"/>
                                    </otm:EquipmentTag1>
                                    <otm:Status>
                                        <xsl:if
                                            test="//availableEquipment/equipment/status/id/@appCode != '' and //availableEquipment/equipment/status/type != ''">
                                            <otm:StatusTypeGid>
                                                <otm:Gid>
                                                  <otm:DomainName>
                                                  <xsl:value-of
                                                  select="//availableEquipment/equipment/status/id/@appCode"
                                                  />
                                                  </otm:DomainName>
                                                  <otm:Xid>EQUIPOS</otm:Xid>
                                                </otm:Gid>
                                            </otm:StatusTypeGid>
                                        </xsl:if>

                                        <otm:StatusValueGid>
                                            <xsl:variable name="isAC" select="//availableEquipment/equipment/isActive"/>
                                            <otm:Gid>
                                                <otm:DomainName>
                                                  <xsl:value-of
                                                  select="//availableEquipment/equipment/status/id/@appCode"
                                                  />
                                                </otm:DomainName>
                                                <otm:Xid>
                                                    <xsl:for-each select="$activeE">
                                                        <xsl:value-of select="key('acti',$isAC)"/>
                                                    </xsl:for-each>
                                                </otm:Xid>
                                            </otm:Gid>
                                        </otm:StatusValueGid>
                                    </otm:Status>
                                    <otm:Status>
                                        <xsl:if
                                            test="//availableEquipment/equipment/status/id/@appCode != '' and //availableEquipment/equipment/status/type != ''">
                                            <otm:StatusTypeGid>
                                                <otm:Gid>
                                                  <otm:DomainName>
                                                  <xsl:value-of
                                                  select="//availableEquipment/equipment/status/id/@appCode"
                                                  />
                                                  </otm:DomainName>
                                                  <otm:Xid>STATUS_EQ</otm:Xid>
                                                </otm:Gid>
                                            </otm:StatusTypeGid>
                                        </xsl:if>

                                        <otm:StatusValueGid>
                                            <otm:Gid>
                                                <otm:DomainName>
                                                  <xsl:value-of
                                                  select="//availableEquipment/equipment/status/id/@appCode"
                                                  />
                                                </otm:DomainName>
                                                <otm:Xid>
                                                    <xsl:variable name="estatID" select="//availableEquipment/equipment/status/value"/>
                                                    <xsl:for-each select="$estE">
                                                        <xsl:value-of select="key('estatus',$estatID)"/>
                                                    </xsl:for-each>
                                                </otm:Xid>
                                            </otm:Gid>
                                        </otm:StatusValueGid>
                                    </otm:Status>
                                    <otm:FlexFieldStrings/>
                                    <otm:FlexFieldNumbers/>
                                    <otm:FlexFieldDates/>
                                </otm:Equipment>
                            </GLogXMLElement>
                        </TransmissionBody>
                    </Transmission>
                </tran:publish>
            </soapenv:Body>
        </soapenv:Envelope>
    </xsl:template>
</xsl:stylesheet>
