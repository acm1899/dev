<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="/">
        <result continue="true">
            <data name="dominio">
                <xsl:value-of
                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitGid/*:Gid/*:DomainName"
                />
            </data>  
            <data name="id">
                <xsl:value-of
                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitGid/*:Gid/*:Xid"
                />
            </data>
        </result>
    </xsl:template>
</xsl:stylesheet>