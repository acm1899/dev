<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:tran="http://xmlns.oracle.com/apps/otm/TransmissionService"
            xmlns:v6="http://xmlns.oracle.com/apps/otm/transmission/v6.4">
            <soapenv:Header>
                <wsse:Security
                    xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                    <wsse:UsernameToken
                        xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                        <wsse:Username>MASTER.IVAN_ROSALES</wsse:Username>
                        <wsse:Password
                            Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"
                            >Sol140218357</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
            </soapenv:Header>
            <soapenv:Body>
                <tran:publish>
                    <Transmission xmlns="http://xmlns.oracle.com/apps/otm/transmission/v6.4">
                        <TransmissionHeader> </TransmissionHeader>
                        <TransmissionBody>
                            <GLogXMLElement>
                                <otm:ShipmentStatus
                                    xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4"
                                    xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4">
                                    <otm:ShipmentGid>
                                        <otm:Gid>
                                            <otm:DomainName>
                                                <xsl:value-of select="//event/id/@appCode"/>
                                            </otm:DomainName>
                                            <otm:Xid>
                                                <xsl:value-of select="//event/travel/id"/>
                                            </otm:Xid>
                                        </otm:Gid>
                                    </otm:ShipmentGid>
                                    <otm:StatusCodeGid>
                                        <otm:Gid>
                                            <otm:DomainName>MASTER</otm:DomainName>
                                            <otm:Xid>
                                                <xsl:variable name="clean" select="//event/id"/>
                                                <xsl:choose>
                                                    <xsl:when test="not(contains($clean,'.'))">
                                                        <xsl:value-of select="$clean"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:variable name="nameC"
                                                            select="substring-after($clean, '.')"/>
                                                        <xsl:value-of select="$nameC"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                                
                                            </otm:Xid>
                                        </otm:Gid>
                                    </otm:StatusCodeGid>
                                    <otm:SStatusSEquipment>
                                        <xsl:variable name="equipStatus"
                                            select="//message/event/transportation/truck/equipments/equipment/code"/>
                                        <xsl:variable name="clean"
                                            select="substring-after($equipStatus, '.')"/>
                                        <xsl:if test="$equipStatus">
                                            <otm:EquipmentGid>
                                                <otm:Gid>
                                                    <otm:DomainName>MASTER</otm:DomainName>
                                                    <otm:Xid>
                                                        <xsl:value-of select="$clean"/>
                                                    </otm:Xid>
                                                </otm:Gid>
                                            </otm:EquipmentGid>
                                        </xsl:if>
                                    </otm:SStatusSEquipment>
                                    <xsl:if test="//message/event/transportation/truck/id">
                                        <otm:PowerUnitGid>
                                            <xsl:variable name="idtruck" select="//event/transportation/truck/id"/>
                                            <xsl:variable name="truckclean" select="substring-after($idtruck,'.')"/>
                                            <otm:Gid>
                                                <otm:DomainName>MASTER</otm:DomainName>
                                                <otm:Xid>
                                                    <xsl:value-of
                                                        select="$truckclean"/>
                                                </otm:Xid>
                                            </otm:Gid>
                                        </otm:PowerUnitGid>
                                    </xsl:if>
                                    <xsl:choose>
                                        <xsl:when test="//event/travel/locations/location">
                                            <xsl:for-each select="//event/travel/locations/location">
                                                <otm:SSStop>
                                                    <xsl:variable name="pos" select="position()"/>
                                                    <otm:SSSTopSequenceNum>
                                                        <xsl:value-of select="$pos"/>
                                                    </otm:SSSTopSequenceNum>
                                                    <otm:SSLocation>
                                                        <otm:LocationID>
                                                            <xsl:value-of
                                                                select="//event/travel/locations[$pos]/location/id"
                                                            />
                                                        </otm:LocationID>
                                                        <otm:Latitude>
                                                            <xsl:value-of
                                                                select="//message/event/address/latitude"/>
                                                        </otm:Latitude>
                                                        <otm:Longitude>
                                                            <xsl:value-of
                                                                select="//message/event/address/longitude"/>
                                                        </otm:Longitude>
                                                    </otm:SSLocation>
                                                </otm:SSStop>
                                            </xsl:for-each>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:if test="//message/event/address">
                                                <otm:SSStop>
                                                    <otm:SSSTopSequenceNum>
                                                        <xsl:value-of
                                                            select="//event/travel/stops/stop/numberStop"/>
                                                    </otm:SSSTopSequenceNum>
                                                    <otm:SSLocation>
                                                        <otm:Latitude>
                                                            <xsl:value-of
                                                                select="//message/event/address/latitude"/>
                                                        </otm:Latitude>
                                                        <otm:Longitude>
                                                            <xsl:value-of
                                                                select="//message/event/address/longitude"/>
                                                        </otm:Longitude>
                                                    </otm:SSLocation>
                                                </otm:SSStop>
                                            </xsl:if>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <otm:StatusGroup>
                                        <otm:StatusGroupGid>
                                            <otm:Gid>
                                                <otm:DomainName>MASTER</otm:DomainName>
                                                <otm:Xid>
                                                    <xsl:variable name="nombre"
                                                        select="//event/eventGroup/name"/>
                                                    <xsl:choose>
                                                        <xsl:when test="not(contains($nombre,'.'))">
                                                            <xsl:value-of select="$nombre"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:variable name="group"
                                                                select="substring-after($nombre, '.')"/>
                                                            <xsl:value-of select="$group"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                   
                                                  
                                                   
                                                </otm:Xid>
                                            </otm:Gid>
                                        </otm:StatusGroupGid>
                                        
                                        <otm:StatusGroupDescription>
                                            <xsl:value-of select="//event/eventGroup/description"/>
                                        </otm:StatusGroupDescription>
                                        
                                    </otm:StatusGroup>
                                    
                                    <otm:ResponsiblePartyGid>
                                        <otm:Gid>
                                            <otm:Xid>
                                                <xsl:value-of select="//event/user/description"/>
                                            </otm:Xid>
                                        </otm:Gid>
                                    </otm:ResponsiblePartyGid>
                                    <otm:SSContact>
                                        <otm:SSContactName>
                                            <xsl:value-of select="//event/user/name"/>
                                        </otm:SSContactName>
                                    </otm:SSContact>
                                    <xsl:if test="//message/event/comments">
                                        
                                        <xsl:for-each select="//message/event/comments">
                                            <xsl:variable name="pos" select="position()"/>
                                            <otm:Remark>
                                                <otm:RemarkSequence><xsl:value-of select="$pos"/></otm:RemarkSequence>
                                                <otm:RemarkQualifierGid>
                                                    <otm:TransactionCode>I</otm:TransactionCode>
                                                    <otm:Gid>
                                                        <otm:Xid>ADD_INFOS</otm:Xid>
                                                    </otm:Gid>
                                                    
                                                </otm:RemarkQualifierGid>
                                                <otm:RemarkText>
                                                    <xsl:value-of select="//message/event/comments[$pos]/text"/>
                                                </otm:RemarkText>
                                            </otm:Remark>
                                            
                                        </xsl:for-each>
                                        
                                        
                                    </xsl:if>
                                    
                                    
                                    <otm:EventDt>
                                        <xsl:variable name="dateMaster" select="//event/dateStart"/>
                                        <xsl:choose>
                                            <xsl:when test="$dateMaster">
                                                <xsl:variable name="date"
                                                    select="replace($dateMaster, '[/,\s,:]', '')"/>
                                                <xsl:variable name="year"
                                                    select="substring($date, 5, 4)"/>
                                                <xsl:variable name="mes"
                                                    select="substring($date, 3, 2)"/>
                                                <xsl:variable name="day"
                                                    select="substring($date, 0, 3)"/>
                                                <xsl:variable name="hour"
                                                    select="substring($date, 9, 2)"/>
                                                <xsl:variable name="min"
                                                    select="substring($date, 11, 2)"/>
                                                <xsl:variable name="sec"
                                                    select="substring($date, 13, 2)"/>
                                                
                                                
                                                <xsl:variable name="newDate"
                                                    select="concat($year, '-', $mes, '-', $day)"/>
                                                <xsl:variable name="newTime"
                                                    select="concat($hour, ':', $min, ':', $sec)"/>
                                                <xsl:variable name="ddtt"
                                                    select="dateTime(xs:date($newDate), xs:time($newTime))"/>
                                                <xsl:variable name="dateOtm"
                                                    select="format-dateTime($ddtt, '[Y0001][M01][D01][H01][m01][s01]')"/>
                                                <otm:GLogDate>
                                                    <xsl:value-of select="$dateOtm"/>
                                                </otm:GLogDate>
                                                <otm:TZId>Mexico/General</otm:TZId>
                                                <otm:TZOffset>-05:00</otm:TZOffset>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <otm:GlogDate/>
                                                <otm:TZId>Mexico/General</otm:TZId>
                                                <otm:TZOffset>-05:00</otm:TZOffset>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </otm:EventDt>
                                </otm:ShipmentStatus>
                            </GLogXMLElement>
                        </TransmissionBody>
                    </Transmission>
                </tran:publish>
            </soapenv:Body>
        </soapenv:Envelope>
    </xsl:template>
    
</xsl:stylesheet>