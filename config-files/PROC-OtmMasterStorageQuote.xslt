<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="/">
        <result continue="true">
            <data name="dominio">MASTER</data>  
            <data name="id">
                <xsl:value-of select="//quote/transaction/IDClienteMasterAux_t"/>
            </data>
        </result>
    </xsl:template>
</xsl:stylesheet>