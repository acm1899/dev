<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output indent="yes"/>
    <!--  Status  -->
    <xsl:key name="status" match="lookup/status/string" use="@id"/>
    <xsl:variable name="trucSta" select='document("truckStatus.xml")/lookup/status'/>

    <!--  Marca  -->
    <xsl:key name="marca" match="marca/string" use="@id"/>
    <xsl:variable name="marcaT" select='document("marca.xml")/marca'/>

    <!--  Proposito  -->
    <xsl:key name="prop" match="proposito/string" use="@id"/>
    <xsl:variable name="porpT" select='document("proposito.xml")/proposito'/>
    
    <!--  Description  -->
    <xsl:key name="descr" match="desc/string" use="@id"/>
    <xsl:variable name="descrT" select='document("descript.xml")/desc'/>

    <xsl:template match="/">
        <message>
            <xsl:variable name="date"
                select="string(/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:DateBuilt/*:GLogDate)"/>
            <availableTruck>
                <id appCode="MASTER">
                    <xsl:attribute name="appCode">
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitGid/*:Gid/*:DomainName"
                        />
                    </xsl:attribute>
                </id>
                <createDate>
                    <xsl:if test="$date">
                        <xsl:variable name="year" select="substring($date, 0, 5)"/>
                        <xsl:variable name="mes" select="substring($date, 5, 2)"/>
                        <xsl:variable name="dia" select="substring($date, 7, 2)"/>
                        <xsl:variable name="hora" select="substring($date, 9, 2)"/>
                        <xsl:variable name="minuto" select="substring($date, 11, 2)"/>
                        <xsl:variable name="seg" select="substring($date, 12, 2)"/>
                        
                        <xsl:variable name="newDate" select="concat($year, '-', $mes, '-', $dia)"/>
                        
                        <xsl:variable name="tiemp" select="concat($hora, ':', $minuto, ':', $seg)"/>
                        
                        <xsl:variable name="timeStamp"
                            select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                        <xsl:variable name="fecha2" as="xs:string"
                            select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                        <xsl:value-of select="$fecha2"/>
                    </xsl:if>
                   
                </createDate>
                <truck>
                    <id>
                        <xsl:attribute name="type">
                            <xsl:variable name="type"
                                select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:PowerUnitTypeGid/*:Gid/*:Xid"/>
                            <xsl:for-each select="$marcaT">
                                <xsl:value-of select="key('marca', $type)"/>
                            </xsl:for-each>
                        </xsl:attribute>
                        <xsl:attribute name="typeName"></xsl:attribute>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitGid/*:Gid/*:Xid"
                        />
                    </id>
                    <mileage>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldNumbers/*:AttributeNumber3"
                        />
                    </mileage>
                    <plaque>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:PowerUnitNum"
                        />
                    </plaque>
                    <createDate>
                        <xsl:variable name="date"
                            select="string(/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:DateBuilt/*:GLogDate)"/>
                        <xsl:if test="$date">
                            <xsl:variable name="year" select="substring($date, 0, 5)"/>
                            <xsl:variable name="mes" select="substring($date, 5, 2)"/>
                            <xsl:variable name="dia" select="substring($date, 7, 2)"/>
                            <xsl:variable name="hora" select="substring($date, 9, 2)"/>
                            <xsl:variable name="minuto" select="substring($date, 11, 2)"/>
                            <xsl:variable name="seg" select="substring($date, 12, 2)"/>
                            
                            <xsl:variable name="newDate" select="concat($year, '-', $mes, '-', $dia)"/>
                            
                            <xsl:variable name="tiemp" select="concat($hora, ':', $minuto, ':', $seg)"/>
                            
                            <xsl:variable name="timeStamp"
                                select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                            <xsl:variable name="fecha2" as="xs:string"
                                select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                            <xsl:value-of select="$fecha2"/>
                        </xsl:if>
                       
                    </createDate>
                    <brand>
                        <xsl:variable name="type"
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:PowerUnitTypeGid/*:Gid/*:Xid"/>
                       <xsl:value-of select="$type"/>
                    </brand>
                    <serialNumber>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldStrings/*:Attribute1"
                        />
                    </serialNumber>
                    <iave>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldStrings/*:Attribute2"
                        />
                    </iave>
                    <engineNumber>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldNumbers/*:AttributeNumber1"
                        />
                    </engineNumber>
                    <litreCapacity>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldNumbers/*:AttributeNumber2"
                        />
                    </litreCapacity>
                    <carryingCapacity>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldNumbers/*:AttributeNumber6"
                        />
                    </carryingCapacity>
                    <rfid/>
                    <description>
                        <id>
                            <xsl:variable name="desc" select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:Description"/>
                            <xsl:variable name="nId" select="replace($desc,'\s','_')"/>
                            <xsl:for-each select="$descrT">
                                <xsl:value-of select="key('descr',$nId)"/>
                            </xsl:for-each>
                        </id>
                        <name>
                            <xsl:value-of
                                select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:Description"
                            />
                        </name>
                    </description>
                    <isActive>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:IsActive"
                        />
                    </isActive>
                    <useAccesory>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldStrings/*:Attribute7"
                        />
                    </useAccesory>
                    <fuelLoad>
                        <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldStrings/*:Attribute5"
                        />
                    </fuelLoad>
                    <km>
                        <local>
                            <xsl:value-of
                                select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldNumbers/*:AttributeNumber5"
                            />
                        </local>
                        <foreign>
                            <xsl:value-of
                                select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldNumbers/*:AttributeNumber4"
                            />
                        </foreign>
                    </km>
                    <dimensions>
                        <depth>0</depth>
                        <height>0</height>
                        <width>0</width>
                    </dimensions>
                    <doors>
                        <left>S</left>
                        <rigth>S</rigth>
                        <back>N</back>
                        <front>S</front>
                    </doors>
                    <xsl:choose>
                        <xsl:when
                            test="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:Status/*:StatusValueGid/*:Gid/*:Xid">
                            <status id="">
                                <xsl:attribute name="appCode">
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitGid/*:Gid/*:DomainName"
                                    />
                                </xsl:attribute>
                                <xsl:variable name="idStatus"
                                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:Status/*:StatusValueGid/*:Gid/*:Xid"/>
                                <id>
                                    <xsl:for-each select="$trucSta">
                                        <xsl:value-of select="key('status', $idStatus)"/>
                                    </xsl:for-each>
                                </id>
                                <type>MASTER.TRACTO_STATUS</type>
                                <name/>
                                <value>
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:Status/*:StatusValueGid/*:Gid/*:Xid"
                                    />
                                </value>
                            </status>
                        </xsl:when>
                        <xsl:otherwise>
                            <status id="" >
                                <xsl:attribute name="appCode">
                                    <xsl:value-of
                                        select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitGid/*:Gid/*:DomainName"
                                    />
                                </xsl:attribute>
                                <id>2</id>
                                <type>MASTER.TRACTO_STATUS</type>
                                <name/>
                                <value>TRACTO_NO_ASIGNADO</value>
                            </status>
                        </xsl:otherwise>
                    </xsl:choose>
                    <porpouse>
                        <id>
                            <xsl:variable name="prop" select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldStrings/*:Attribute4"/>
                            <xsl:for-each select="$porpT">
                                <xsl:value-of select="key('prop',$prop)"/>
                            </xsl:for-each>    
                            
                        </id>
                        <name> <xsl:value-of
                            select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldStrings/*:Attribute4"
                        /></name>
                        <description/>
                    </porpouse>
                </truck>
                <comments>
                    <xsl:choose>
                        <xsl:when test="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldStrings/*:Attribute6">
                            <id>1</id>
                            <text>
                                <xsl:value-of
                                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:PowerUnit/*:PowerUnitHeader/*:FlexFieldStrings/*:Attribute6"
                                />
                            </text>
                        </xsl:when>
                        <xsl:otherwise>
                            <id>1</id>
                            <text>Observaciones</text>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    
                </comments>
            
            
            </availableTruck>
            

        </message>

    </xsl:template>
</xsl:stylesheet>
